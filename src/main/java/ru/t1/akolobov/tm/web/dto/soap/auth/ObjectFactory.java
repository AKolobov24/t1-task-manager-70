package ru.t1.akolobov.tm.web.dto.soap.auth;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

    private final static QName _LoginRequest_QNAME = new QName("http://t1.ru/akolobov/tm/web/dto/soap/auth", "loginRequest");
    private final static QName _LoginResponse_QNAME = new QName("http://t1.ru/akolobov/tm/web/dto/soap/auth", "loginResponse");
    private final static QName _LogoutRequest_QNAME = new QName("http://t1.ru/akolobov/tm/web/dto/soap/auth", "logoutRequest");
    private final static QName _LogoutResponse_QNAME = new QName("http://t1.ru/akolobov/tm/web/dto/soap/auth", "logoutResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.t1.akolobov.tm.web.dto.soap.project
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginRequest }
     */
    public LoginRequest createLoginRequest() {
        return new LoginRequest();
    }

    /**
     * Create an instance of {@link LoginResponse }
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link LogoutRequest }
     */
    public LogoutRequest createLogoutRequest() {
        return new LogoutRequest();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }


    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginRequest }{@code >}}
     */
    @XmlElementDecl(namespace = "http://t1.ru/akolobov/tm/web/dto/soap/auth", name = "loginRequest")
    public JAXBElement<LoginRequest> createLoginRequest(LoginRequest value) {
        return new JAXBElement<LoginRequest>(_LoginRequest_QNAME, LoginRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://t1.ru/akolobov/tm/web/dto/soap/auth", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutRequest }{@code >}}
     */
    @XmlElementDecl(namespace = "http://t1.ru/akolobov/tm/web/dto/soap/auth", name = "logoutRequest")
    public JAXBElement<LogoutRequest> createLogoutRequest(LogoutRequest value) {
        return new JAXBElement<LogoutRequest>(_LogoutRequest_QNAME, LogoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://t1.ru/akolobov/tm/web/dto/soap/auth", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

}
