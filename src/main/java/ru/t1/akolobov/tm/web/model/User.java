package ru.t1.akolobov.tm.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@XmlType(name = "user")
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "tm_user", schema = "web")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @XmlElement(name = "login")
    private String login;

    @JsonIgnore
    private String passwordHash;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    public User() {
    }

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final List<Role> roles) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roles = roles;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull final String login) {
        this.login = login;
    }

    @NotNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @NotNull
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(@NotNull final List<Role> roles) {
        this.roles = roles;
    }

}
