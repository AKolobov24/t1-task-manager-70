package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.ITaskRestEndpoint;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.util.UserUtil;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public final class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public Task findById(
            @PathVariable("id")
            @NotNull final String id
    ) {
        return taskService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public Task save(
            @RequestBody
            @NotNull final Task task
    ) {
        task.setUser(UserUtil.getUser());
        return taskService.save(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(
            @PathVariable("id")
            @NotNull final String id
    ) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

}
