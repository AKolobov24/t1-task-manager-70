package ru.t1.akolobov.tm.web.client;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;

public class AuthRequestInterceptor implements RequestInterceptor {

    @NotNull
    private String sessionCookie;

    public AuthRequestInterceptor(@NotNull final String sessionCookie) {
        this.sessionCookie = sessionCookie;
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(HttpHeaders.COOKIE, "JSESSIONID=" + sessionCookie);
    }

    @NotNull
    public String getSessionCookie() {
        return sessionCookie;
    }

    public void setSessionCookie(@NotNull final String sessionCookie) {
        this.sessionCookie = sessionCookie;
    }

}
