package ru.t1.akolobov.tm.web.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseDDLAuto();

    @NotNull
    String getDatabaseShowSQL();

    @NotNull
    String getSecondLvlCache();

    @NotNull
    String getFactoryClass();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getRegionPrefix();

    @NotNull
    String getConfigFilePath();

}
