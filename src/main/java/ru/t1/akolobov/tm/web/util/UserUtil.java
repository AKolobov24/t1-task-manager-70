package ru.t1.akolobov.tm.web.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.akolobov.tm.web.dto.CustomUser;
import ru.t1.akolobov.tm.web.model.User;

public class UserUtil {

    private UserUtil() {
    }

    @NotNull
    public static User getUser() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("Access denied!");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("Access denied!");
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getUser();
    }

    @NotNull
    public static String getUserId() {
        return UserUtil.getUser().getId();
    }

}
