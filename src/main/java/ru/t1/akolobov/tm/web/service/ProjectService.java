package ru.t1.akolobov.tm.web.service;

import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.model.Project;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

}
