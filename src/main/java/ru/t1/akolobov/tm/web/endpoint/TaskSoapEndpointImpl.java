package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.dto.soap.task.*;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.util.UserUtil;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";
    public final static String NAMESPACE = "http://t1.ru/akolobov/tm/web/dto/soap/task";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "findAllTasksRequest", namespace = NAMESPACE)
    public FindAllTasksResponse findAllTasks(
            @NotNull
            @RequestPayload final FindAllTasksRequest request
    ) {
        @NotNull final FindAllTasksResponse response = new FindAllTasksResponse();
        response.getReturn().addAll(taskService.findAllByUserId(UserUtil.getUserId()));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "findTaskByIdRequest", namespace = NAMESPACE)
    public FindTaskByIdResponse findTaskById(
            @NotNull
            @RequestPayload final FindTaskByIdRequest request
    ) {
        @NotNull final FindTaskByIdResponse response = new FindTaskByIdResponse();
        response.setReturn(
                taskService.findByUserIdAndId(
                        UserUtil.getUserId(),
                        request.getId()
                )
        );
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "saveTaskRequest", namespace = NAMESPACE)
    public SaveTaskResponse saveTask(
            @NotNull
            @RequestPayload final SaveTaskRequest request
    ) {
        @NotNull final SaveTaskResponse response = new SaveTaskResponse();
        @NotNull final Task task = request.getTask();
        task.setUser(UserUtil.getUser());
        response.setReturn(taskService.save(task));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "deleteTaskByIdRequest", namespace = NAMESPACE)
    public DeleteTaskByIdResponse deleteTask(
            @NotNull
            @RequestPayload final DeleteTaskByIdRequest request
    ) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new DeleteTaskByIdResponse();
    }

}
