package ru.t1.akolobov.tm.web.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.model.AbstractModel;

import java.util.Collection;

public interface IUserOwnedService<M extends AbstractModel> extends IService<M> {

    @NotNull
    Collection<M> findAllByUserId(@NotNull final String userId);

    @Nullable
    M findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
