package ru.t1.akolobov.tm.web.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.web.model.Project;

@Repository
@Scope("prototype")
public interface ProjectRepository extends CommonRepository<Project> {

}
