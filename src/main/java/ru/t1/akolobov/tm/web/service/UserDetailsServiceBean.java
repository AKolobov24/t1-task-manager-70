package ru.t1.akolobov.tm.web.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.web.dto.CustomUser;
import ru.t1.akolobov.tm.web.enumerated.RoleType;
import ru.t1.akolobov.tm.web.model.Role;
import ru.t1.akolobov.tm.web.model.User;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userService.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        @NotNull final List<String> roles = user
                .getRoles()
                .stream()
                .map(Role::toString)
                .collect(Collectors.toList());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()
        ).withUser(user);
    }

    @PostConstruct
    public void init() {
        createUser("admin", "admin", RoleType.ADMIN);
        createUser("user", "user", RoleType.USER);
    }

    public void createUser(@NotNull final String login,
                           @NotNull final String password,
                           @NotNull final RoleType roleType
    ) {
        if (userService.existsByLogin(login)) return;
        @NotNull final User user = new User(login, passwordEncoder.encode(password));
        @NotNull final Role role = new Role(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userService.save(user);
    }

}
